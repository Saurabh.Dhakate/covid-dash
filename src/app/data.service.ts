import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  data: any = []
  cardData: any = []
  filterData: any = []
  tableData: any = []
  day: String | undefined
  themes: any = {
    "grey": {
      dark: 'grey',
      light: "grey-light"
    }, "red": {
      dark: 'red',
      light: "red-light"
    }, "green": {
      dark: 'green',
      light: "green-light"
    }, "blue": {
      dark: 'blue',
      light: "blue-light"
    }, "indigo": {
      dark: 'indigo',
      light: "indigo-light"
    }, "pink": {
      dark: 'pink',
      light: "pink-light"
    }, "purple": {
      dark: 'purple',
      light: "purple-light"
    }
  }
  themeColor: any = this.themes.grey

  constructor(private http: HttpClient) { }

  getGlobalData(): void {

    this.http.get('https://covid-193.p.rapidapi.com/statistics', {
      headers: {
        'x-rapidapi-key': '89afa7ece5msh53dafe759b97cd7p1b7aa5jsn4e1c453e0c05',
        'x-rapidapi-host': 'covid-193.p.rapidapi.com'
      }
    })
      .subscribe({
        next: (data: any) => {
          this.day = data['response'][0]['day']
          this.data = data['response']
          this.tableData.length = 0
          data['response'].map((data: any) => {
            this.tableData.push({
              country: data.country,
              totalCases: data.cases['total'] ? data.cases['total'].toLocaleString('en-IN') : 0,
              activeCases: data.cases['active'] ? data.cases['active'].toLocaleString('en-IN') : 0,
              recoveredCases: data.cases['recovered'] ? data.cases['recovered'].toLocaleString('en-IN') : 0,
              criticalCases: data.cases['critical'] ? data.cases['critical'].toLocaleString('en-IN') : 0,
              totalDeaths: data.deaths['total'] ? data.deaths['total'].toLocaleString('en-IN') : 0
            })
            this.filterData = [...this.tableData]
          })
          this.cardData.length = 0
          this.cardData = [
            {
              cat: 'Total Cases',
              val: data['response'].reduce((acc: any, cv: any) => acc + cv.cases['total'], 0)
            },
            {
              cat: 'Active Cases',
              val: data['response'].reduce((acc: any, cv: any) => acc + cv.cases['active'], 0)
            },
            {
              cat: 'Recovered Cases',
              val: data['response'].reduce((acc: any, cv: any) => acc + cv.cases['recovered'], 0)
            },
            {
              cat: 'Critical Cases',
              val: data['response'].reduce((acc: any, cv: any) => acc + cv.cases['critical'], 0)
            },
            {
              cat: 'Total Deaths',
              val: data['response'].reduce((acc: any, cv: any) => acc + cv.deaths['total'], 0)
            }
          ]
        },
        error: () => console.log('error')
      })

  }

  filterTable(query: String) {
    this.filterData = this.tableData.filter((item: any) => (item['country'].toLowerCase().includes(query.toLowerCase())))
  }

  getCountryData(e: any): void {
    for (let data of this.data) {
      if (data['country'] === e.target.value) {

        this.tableData.length = 0
        this.tableData.push({
          country: data.country,
          totalCases: data.cases['total'] ? data.cases['total'].toLocaleString('en-IN') : 0,
          activeCases: data.cases['active'] ? data.cases['active'].toLocaleString('en-IN') : 0,
          recoveredCases: data.cases['recovered'] ? data.cases['recovered'].toLocaleString('en-IN') : 0,
          criticalCases: data.cases['critical'] ? data.cases['critical'].toLocaleString('en-IN') : 0,
          totalDeaths: data.deaths['total'] ? data.deaths['total'].toLocaleString('en-IN') : 0
        })
        this.filterData = [...this.tableData]

        this.cardData.length = 0
        this.cardData = [
          {
            cat: 'Total Cases',
            val: data['cases']['total'] || 0
          }, {
            cat: 'Active Cases',
            val: data['cases']['active'] || 0
          }, {
            cat: 'Recovered Cases',
            val: data['cases']['recovered'] || 0
          }, {
            cat: 'Critical Cases',
            val: data['cases']['critical'] || 0
          }, {
            cat: 'Total Deaths',
            val: data['deaths']['total'] || 0
          }
        ]
      }
    }
  }

  setThemeColor(e: any) {
    this.themeColor = this.themes[e.target.value]
  }
}
