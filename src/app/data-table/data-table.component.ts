import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.css']
})
export class DataTableComponent implements OnInit {

  constructor(public dataService: DataService) { }
  query: any
  gridApi:any
  ngOnInit(): void {

  }
  onGridReady(event:any) {
    this.gridApi = event.api;
    this.gridApi.sizeColumnsToFit();
    this.gridApi.setDomLayout('autoHeight')
  }
  filterTable() {
    this.dataService.filterTable(this.query)
  }

  columnDefs = [
    { field: 'country' },
    { field: 'totalCases' },
    { field: 'activeCases' },
    { field: 'criticalCases' },
    { field: 'recoveredCases' },
    { field: 'totalDeaths' }
  ];

}
