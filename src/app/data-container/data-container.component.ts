import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service'

@Component({
  selector: 'app-data-container',
  templateUrl: './data-container.component.html',
  styleUrls: ['./data-container.component.css']
})
export class DataContainerComponent implements OnInit {

  constructor(public dataService: DataService) { }
  ngOnInit(): void {
    this.dataService.getGlobalData()
  }

}
