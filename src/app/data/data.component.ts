import { Component, OnInit ,Input} from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.css']
})
export class DataComponent implements OnInit {

  @Input() data:any
  constructor(public dataService:DataService) { }

  ngOnInit(): void {
  }

}
